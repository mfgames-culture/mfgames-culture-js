import * as Big from "big.js";
import { Calendar } from "./Calendar";
import { toBig } from "./common";
import * as interfaces from "./interfaces";
import { Instant, InstantOrPeriod, Period } from "./interfaces";

export class Culture
{
    public instants: Calendar<interfaces.Instant>;
    public periods: Calendar<interfaces.Period>;
    private data: interfaces.CultureData;

    constructor(data: interfaces.CultureData)
    {
        // Verify that we have the cultural data and save it.
        if (!data) { throw new Error("Cannot create a culture without "); }
        this.data = data;

        // Make sure we have the proper instants and fill in missing fields.
        data.instants = data.instants || {} as interfaces.CultureCalendarData;
        data.instants.calendarData =
            data.instants.calendarData || {} as interfaces.CalendarData;
        data.instants.calendarData.cycles =
            data.instants.calendarData.cycles || [];

        this.instants = new Calendar<interfaces.Instant>(
            interfaces.TemporalType.instant,
            data.instants.calendarData);

        // Make sure we have proper periods with filled in missing fields.
        data.periods = data.periods || {} as interfaces.CultureCalendarData;
        data.periods.calendarData =
            data.periods.calendarData || {} as interfaces.CalendarData;
        data.periods.calendarData.cycles =
            data.periods.calendarData.cycles || [];

        this.periods = new Calendar<interfaces.Period>(
            interfaces.TemporalType.period,
            data.periods.calendarData);
    }

    public format(
        instantOrPeriod: interfaces.InstantOrPeriod,
        formatId: string): string
    {
        switch (instantOrPeriod.type)
        {
            case interfaces.TemporalType.instant:
                return this.formatInstant(
                    instantOrPeriod as interfaces.Instant,
                    formatId);
            case interfaces.TemporalType.period:
                return this.formatPeriod(
                    instantOrPeriod as interfaces.Period,
                    formatId);
            default:
                throw new Error(
                    "Cannot format unknown type: " + instantOrPeriod.type);
        }
    }

    public formatInstant(
        instant: interfaces.Instant,
        formatId: string): string
    {
        if (!this.data.instants)
        {
            throw new Error("Format data is not loaded.");
        }

        return this.formatInstantOrPeriod(
            instant,
            this.data.instants.formats,
            formatId);
    }

    public formatPeriod(
        period: interfaces.Period,
        formatId: string): string
    {
        if (!this.data.periods)
        {
            throw new Error("Format data is not loaded.");
        }

        return this.formatInstantOrPeriod(
            period,
            this.data.periods.formats,
            formatId);
    }

    public parseInstant(input: any, formatId?: string): interfaces.Instant
    {
        if (!this.data.instants)
        {
            throw new Error("Format data is not loaded.");
        }

        return this.parseInstantOrPeriod<Instant>(
            input,
            formatId,
            this.instants,
            this.data.instants);
    }

    public parsePeriod(input: any, formatId?: string): interfaces.Period
    {
        if (!this.data.periods)
        {
            throw new Error("Format data is not loaded.");
        }

        return this.parseInstantOrPeriod<Period>(
            input,
            formatId,
            this.periods,
            this.data.periods);
    }

    public addPeriod(start: Instant, amount: Period): Instant
    {
        const julian = start.julian.plus(amount.julian);
        const instant = this.instants.get(julian);
        return instant;
    }

    public getFirst(start: InstantOrPeriod, stop: InstantOrPeriod): Instant
    {
        // Normalize the starts and stops.
        const startInstant = this.normalizeInstant(start, start);
        const stopInstant = this.normalizeInstant(stop, start);

        // Figure out which one to return.
        return startInstant.julian.gt(stopInstant.julian)
            ? stopInstant
            : startInstant;
    }

    public getLast(start: InstantOrPeriod, stop: InstantOrPeriod): Instant
    {
        // Normalize the starts and stops.
        const startInstant = this.normalizeInstant(start, start);
        const stopInstant = this.normalizeInstant(stop, start);

        // Figure out which one to return.
        return startInstant.julian.gt(stopInstant.julian)
            ? startInstant
            : stopInstant;
    }

    public getElapsed(start: Instant, stop: Instant): Period
    {
        const julian = stop.julian.minus(start.julian);
        const period = this.periods.get(julian);
        return period;
    }

    /**
     * Retrieves a list of the known instant formats
     * including `JD` for Julian Day.
     */
    public getInstantFormats(): string[]
    {
        if (!this.data.instants)
        {
            return [];
        }

        return this.getFormats(this.data.instants);
    }

    /**
     * Retrieves a list of the known period formats
     * including `JD` for Julian Day.
     */
    public getPeriodFormats(): string[]
    {
        if (!this.data.periods)
        {
            return [];
        }

        return this.getFormats(this.data.periods);
    }

    /**
     * Retrieves a list of all known formats for instants.
     */
    private getFormats(data: interfaces.CultureCalendarData): string[]
    {
        const formats = ["JD"];

        for (const key in data.formats)
        {
            if (data.formats.hasOwnProperty(key))
            {
                formats.push(key);
            }
        }

        return formats;
    }

    private normalizeInstant(
        start: InstantOrPeriod,
        stop: InstantOrPeriod): Instant
    {
        // If the start is an instant, then we can use that.
        if (start.type === interfaces.TemporalType.instant)
        {
            return start as Instant;
        }

        // The start is a period, see if the start is an period. If it is, then
        // blow up because we can't figure it out.
        if (stop.type === interfaces.TemporalType.period)
        {
            throw new Error("Cannot getFirst or getLast with two periods.");
        }

        // We have an instant to work with.
        return this.addPeriod(stop as Instant, start as Period);
    }

    private parseInstantOrPeriod<T extends interfaces.InstantOrPeriod>(
        input: any,
        formatId: string | undefined,
        calendar: Calendar<T>,
        data: interfaces.CultureCalendarData): T
    {
        // If our input is an instance already, then just use the julian date.
        if (typeof input === "object" && "julian" in input)
        {
            return this.parseInstantOrPeriod<T>(
                input.julian,
                formatId,
                calendar,
                data);
        }

        // If the format ID is given, we retrieve that and determine if we were
        // given a valid input.
        if (formatId)
        {
            if (!(formatId in data.formats))
            {
                throw new Error("Unknown temporal format: " + formatId + ".");
            }
        } else
        {
            // We have to search for the format. We do this by cycling through
            // until we find a regex that matches. We don't need an if-check
            // here because we are working with interfaces.
            let found = false;

            /* tslint:disable */
            for (const fmtId in data.formats)
            {
                const fmt = data.formats[fmtId];
                const fmtRegex = this.getRegex(fmt.elements);

                if (fmtRegex.test(input))
                {
                    formatId = fmtId;
                    found = true;
                    break;
                }
            }
            /* tslint:enable */

            // If we didn't find it, see if this could be something we can
            // parse directly without a format.
            if (!found && this.isNumeric(input))
            {
                const simpleJulian = Big(input);
                const newInstant = this.instants.get(simpleJulian);
                return newInstant as T;
            }

            // If we didn't find it, we have a problem.
            if (!found || !formatId)
            {
                throw new Error("Cannot infer format from " + input + ".");
            }
        }

        // Grab the format and see if this matches the input.
        const formatElements = data.formats[formatId];
        const regex = this.getRegex(formatElements.elements);
        const isMatch = regex.test(input);

        if (!isMatch)
        {
            throw new Error("Cannot find a match for parsing the input: "
                + input + ".");
        }

        // Go through the elements and pull out each one.
        const matches = input.match(regex);
        let matchIndex = 1;
        const instant: any = {};
        let offset = Big(0);

        for (const elem of formatElements.elements)
        {
            // If we have a constant, then skip it.
            if (elem.constant) { continue; }

            // If we have a default, then set the cycles.
            if (elem.default)
            {
                for (const key in elem.default)
                {
                    if (elem.default.hasOwnProperty(key))
                    {
                        instant[key] = elem.default[key];
                    }
                }
            }

            // Pull out the elements and reverse the value.
            const ref = elem.parseRef ? elem.parseRef : elem.ref;

            if (ref)
            {
                // Get the value and then add it to any existing value.
                const value = matches[matchIndex++];
                const cycleIndex = this.getCycleIndex(elem, value);

                if (!(ref in instant)) { instant[ref] = 0; }

                instant[ref] += cycleIndex;
            }

            // If we have a parsing offset, we need to keep track of it
            // so we can avoid double-offsetting when using two calendars.
            if (elem.parseJulianOffset)
            {
                offset = offset.plus(toBig(elem.parseJulianOffset));
            }
        }

        // This is a partial instant, so convert it to Julian and then back into
        // a fully populated object. These are the expensive operations of
        // parsing.
        const julian = calendar.getBigJulian(instant).plus(offset);
        const populatedInstant = calendar.get(julian);
        return populatedInstant;
    }

    private formatInstantOrPeriod(
        instant: interfaces.InstantOrPeriod,
        formats: interfaces.CultureCalendarFormatsData,
        formatId: string): string
    {
        // If we are requesting a Julian Date, then just pass it on.
        if (formatId === "JD")
        {
            return instant.julian.toString();
        }

        // First make sure this is a known format for this culture. If we
        // don't have a format, then try to use the default format.
        if (!formatId && "default" in formats)
        {
            formatId = "default";
        }

        const elements = formats[formatId].elements;

        if (!elements)
        {
            throw new Error("Unknown format ID: " + formatId + ".");
        }

        // We have the format, so build up a string that contains the
        // elements.
        let buffer = "";

        for (const elem of elements)
        {
            // If we are a constant, we can just add it.
            if (elem.constant)
            {
                buffer += elem.constant;
                continue;
            }

            // If we have a reference, then format the cycle.
            if (elem.ref)
            {
                // Get the cycle index.
                const cycleIndex = instant[elem.ref];

                if (cycleIndex === undefined)
                {
                    throw new Error(
                        "Cannot find cycle " + elem.ref
                        + " in " + instant + ".");
                }

                // Add the formatted value to the buffer.
                const value: string = this.formatIndex(elem, cycleIndex);
                buffer += value;
            }
        }

        // Return the resulting text.
        return buffer;
    }

    private isNumeric(input: any)
    {
        // If we are already a big, then we can use it.
        if (input instanceof Big)
        {
            return true;
        }

        // See if we can parse the input.
        const n = parseFloat(input);
        const valid = !!input.match(/^\d+(\.\d+)?$/);
        return valid && !isNaN(n) && isFinite(n);
    }

    private getCycleIndex(
        elem: interfaces.CultureCalendarFormatElementData,
        value: string): number
    {
        // If we have a lookup, then figure out the code.
        if (elem.lookup && elem.maxValue)
        {
            // Loop through all possible values.
            for (let i = 0; i < elem.maxValue; i++)
            {
                const keyValue = this.formatIndex(elem, i);

                if (keyValue.toLowerCase() === value.toLowerCase())
                {
                    return i;
                }
            }

            // If we got this far, we couldn't parse it.
            throw new Error(
                "Cannot parse the string value " + value + " for "
                + elem.ref + ".");
        }

        // Remove the leading zeros and convert it to a number.
        value = value.replace(/^0+/, "");
        value = value ? value : "0";
        let index = parseInt(value, 10);

        // If set have an offset, reverse it.
        if (elem.offset) { index -= elem.offset; }

        // Return the resulting index.
        return index;
    }

    private getRegex(
        elements: interfaces.CultureCalendarFormatElementData[]): RegExp
    {
        // Build up a regular expression from the elements.
        let buffer = "";

        for (const element of elements)
        {
            // If we have a constant, then just add it directly to the regex.
            if (element.constant)
            {
                buffer += element.constant
                    .replace("\\", "\\\\")
                    .replace("/", "\\/")
                    .replace(".", "\\.");
                continue;
            }

            // If we don't have a ref, this is a noop item.
            if (!element.ref)
            {
                continue;
            }

            // If we have a maxValue, then put a simple list.
            if (element.lookup)
            {
                // Make sure we have a maxValue.
                if (!element.maxValue)
                {
                    throw new Error(
                        "Cannot parse a lookup element without maxValue.");
                }

                // Go through the parts and build everything together.
                const lookups = new Array<string>();

                for (let i = 0; i <= element.maxValue; i++)
                {
                    const value: string = this
                        .formatIndex(element, i)
                        .replace("\\", "\\\\")
                        .replace("/", "\\/")
                        .replace(".", "\\.");
                    lookups.push(value);
                }

                // Add the regular expression.
                buffer += "(" + lookups.join("|") + ")";
                continue;
            }

            // If we don't have a lookup, then go with the numeric values.
            if (!element.maxDigits)
            {
                throw new Error(
                    "Cannot parse a value for " + element.ref
                    + " without a maxDigits.");
            }

            if (!element.minDigits)
            {
                throw new Error(
                    "Cannot parse a value for " + element.ref
                    + " without a minDigits.");
            }

            // Create a regex for the numeric value.
            if (element.minDigits === element.maxDigits)
            {
                buffer += "(\\d{" + element.minDigits + "})";
            } else
            {
                buffer += "(\\d{" + element.minDigits
                    + "," + element.maxDigits + "})";
            }
        }

        // Return the resulting buffer.
        buffer = "^" + buffer + "$";
        return new RegExp(buffer, "i");
    }

    private formatIndex(
        elem: interfaces.CultureCalendarFormatElementData,
        cycleIndex: number): string
    {
        // Handle any offset, if we have one.
        if (elem.offset) { cycleIndex += elem.offset; }

        // Convert the results to a string to make it easier to look it up.
        let value = cycleIndex.toString();

        // Get the zero-padding.
        if (elem.minDigits)
        {
            while (value.length < elem.minDigits)
            {
                value = "0" + value;
            }
        }

        // If we have a lookup code, then use the resulting value as
        // a lookup.
        if (elem.lookup)
        {
            value = this.data.lookups[elem.lookup][value];
        }

        // Return the resulting value.
        return value;
    }
}
