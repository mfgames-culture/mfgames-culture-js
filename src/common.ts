import "big.js";
import * as Big from "big.js";
import { CalendarCycleData, CalendarData } from "./interfaces";

/**
 * Converts the input into a Big value. This safely handles a Big being
 * passed into it.
 */
export function toBig(input: any)
    : BigJsLibrary.BigJS
{
    switch (typeof (input))
    {
        case "number":
            const numberInput = input as number;
            const bigNumber = Big(numberInput);
            return bigNumber;

        case "string":
            const stringInput = input as string;
            let bigString: BigJsLibrary.BigJS;

            if (stringInput.indexOf("/") >= 0)
            {
                // Perform this as a symbolic division.
                const parts = stringInput.split("/").map((a) => Big(a.trim()));
                const value = parts[0].div(parts[1]);
                return value;
            } else
            {
                // Just parse the string directly.
                bigString = Big(stringInput);
            }

            return bigString;

        default:
            // If our input was another point, then use the julian inside it.
            if (input.julian)
            {
                return toBig(input.julian);
            }

            // If we have a "isBig", then use that.
            if (input instanceof Big)
            {
                return input;
            }
    }

    // If we drop out of the statement, then we don't know how to convert it.
    throw new Error("Cannot convert '" + input + "' to a BigJS.");
}

/**
 * Returns the fractional day from the given Julian day.
 */
export function getPartialDays(julian: BigJsLibrary.BigJS): BigJsLibrary.BigJS
{
    return julian.mod(1);
}

/**
 * Merges one or more CalendarData objects into a new one. If a single calendar
 * is given, then it will be returned.
 */
export function mergeCalendars(
    calendars: CalendarData[]): CalendarData
{
    // If we didn't get anything, then return an empty object.
    if (!calendars || calendars.length === 0)
    {
        return {} as CalendarData;
    }

    // If we go one, then just return it.
    if (calendars.length === 1)
    {
        return calendars[0];
    }

    // Use the first calendar and combine the results into it. We need to
    // clone the cycles because we modify the cycles below which would then
    // change further uses of the individual calendar or new ones.
    const calendarData = { ...calendars[0] };

    if (calendarData.cycles)
    {
        calendarData.cycles = calendarData.cycles
            .map((c) => ({ ...c })) as CalendarCycleData[];
    }

    // Create a calendar from the resulting cloned data.
    const calendar = calendarData;
    calendar.id = "composite";

    for (let index = 1; index < calendars.length; index++)
    {
        const childCalendar = calendars[index];

        if (childCalendar.cycles)
        {
            for (const cycle of childCalendar.cycles)
            {
                if (!calendar.cycles)
                {
                    calendar.cycles = [];
                }

                calendar.cycles.push(cycle);
            }
        }
    }

    // Return the resulting calendar.
    return calendar;
}
