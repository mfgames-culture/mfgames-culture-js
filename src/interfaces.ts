import * as Big from "big.js";

export enum CycleType
{
    /** A cycle that loops within its constraint to figure out its length. */
    repeat = "repeat",

    /** A cycle that has a length determined by a formula. */
    calculate = "calculate",

    /** A cycle that has a length based its index and a series of lengths. */
    sequence = "sequence",

    /** A cycle that is calculated based on multiples of a fractional amount. */
    fraction = "fraction",
}

export enum ComponentType
{
    /** The component represents a culture. */
    culture = "culture",

    /** The component that represents an instance in time. */
    instant = "instant",

    /** The component represents a time period. */
    period = "period",
}

export enum TemporalType
{
    /** An object that represents a single instant in time. */
    instant = "instant",

    /** An object that represents a period of time or time span. */
    period = "period",
}

export enum CycleOperationType
{
    /**
     * The value of the cycle is an integer division of the referenced cycle.
     */
    div = "div",

    /** The value of the cycle is an integer modulus of the referenced cycle. */
    mod = "mod",

    /** The value of the cycle is a multiplication of the referenced cycle. */
    mult = "mult",
}

export enum FormatType
{
    /** The format represents only a date component. */
    date = "date",

    /** The format represents both a date and time component. */
    dateTime = "dateTime",

    /** The format only has time elements. */
    time = "time",
}

/**
 * Defines the JSON signature for cycle length calculations. This is used by
 * both the repeat and sequence cycles.
 *
 * @interface
 */
export interface CalendarLengthData
{
    /**
     * The amount to adjust a *cycle index* when this length is applied. This
     * must be a whole number (1 or higher integer).
     */
    count: number;

    /**
     * The amount to modify the Julian Day Number when this length is valid for
     * a cycle calculation. Typically `julian` or `single` is required to
     * calculate the length of a cycle.
     */
    julian?: number;

    /**
     * A series of length elements that have various calculations. Only one
     * length within the single is selected, the first one that is valid for
     * the current state.
     */
    single?: CalendarLengthData[];

    /**
     * The reference to a calculated cycle id. This is used with the operation
     * and value properties to determine if this length is valid for the current
     * state. Only cycles that were already calculated, such as parent cycles
     * and ones earlier in the sequence, can be referenced.
     *
     * This is only required with conditional lengths.
     */
    ref?: string;

    /**
     * The operation used to determine if a length is valid. This is used with
     * ref and value. The only values can be "div" and "mod".
     *
     * This is only required with conditional lengths.
     */
    operation?: CycleOperationType;

    /**
     * The value used for the operations and ref properties. The value is
     * determined by the operation, which requires an integer value.
     *
     * This is only required with conditional lengths.
     */
    value?: number;

    /**
     * The value to determine if cycle length calculations should stop looping
     * if the length returned a valid value.
     */
    stopIfValid: boolean;
}

/**
 * Defines the JSON data for a calendar cycle.
 *
 * @interface
 */
export interface CalendarCycleData
{
    /**
     * The identifier for the cycle. This must be unique not only within a
     * single calendar but also all other combined calendars used by a given
     * culture.
     */
    id: string;
    type: CycleType;
    cycles: CalendarCycleData[];
    lengths?: CalendarLengthData[];
    ref?: string;
    value?: number;
    operation?: string;
    offset?: number;
    partialDaysOnly?: boolean;
}

/**
 * Base class for all top-level culture components.
 */
export interface ComponentData
{
    id: string;
    version: number;
    type: ComponentType;
}

export interface CultureCalendarFormatElementData
{
    ref?: string;
    constant?: string;
    default?: any;
    minDigits?: number;
    maxDigits?: number;
    offset?: number;
    lookup?: string;
    maxValue?: number;
    minValue?: number;
    parseRef?: string;

    /**
     * An offset for the resulting Julian date while parsing to avoid doubling
     * changes while using two different calendars.
     */
    parseJulianOffset?: number | string;
}

export interface CultureCalendarFormatData
{
    type: FormatType;
    elements: CultureCalendarFormatElementData[];
}

export interface CultureCalendarFormatsData
{
    [id: string]: CultureCalendarFormatData;
}

export interface CultureCalendarData
{
    refs: string[];
    formats: CultureCalendarFormatsData;

    /**
     * Contains a single CalendarData associated with this culture. This is not
     * typically serialized with the data since it will be populated by the
     * various cultural loaders.
     */
    calendarData?: CalendarData;
}

/**
 * The metadata and top-level information for calendar data including
 * calendars.
 */
export interface CalendarData extends ComponentData
{
    cycles?: CalendarCycleData[];
}

/**
 * The metadata and top-level information for a culture.
 */
export interface CultureData extends ComponentData
{
    instants?: CultureCalendarData;
    periods?: CultureCalendarData;
    lookups: { [id: string]: any };
}

export interface InstantOrPeriod
{
    type: TemporalType;
    julian: BigJsLibrary.BigJS;
}

export interface Instant extends InstantOrPeriod
{
    type: TemporalType.instant;
    [key: string]: any;
}

export interface Period extends InstantOrPeriod
{
    type: TemporalType.period;
    [key: string]: any;
}

/**
 * Defines the signature for an object that provides cultural and
 * calendar data through promises.
 *
 * @interface
 */
export interface PromiseCultureDataProvider
{
    getCalendarPromise(id: string): Promise<CalendarData>;

    /**
     * Returns a promise to a fully-populated culture including the temporal
     * system.
     */
    getCulturePromise(id: string): Promise<CultureData>;
}

/**
 * Defines the signature for an object that provides synchronous cultural and
 * calendar data.
 *
 * @interface
 */
export interface SyncCultureDataProvider
{
    getCalendarSync(id: string): CalendarData;

    /**
     * Returns a fully-populated culture including the temporal system.
     */
    getCultureSync(id: string): CultureData;
}
