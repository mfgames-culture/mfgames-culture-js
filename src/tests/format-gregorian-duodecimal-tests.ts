import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { getJulian, provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    it("can format 2001-01-01 00:00:00 as YYYY-MM-DD HH:mm:ss", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1, 0, 0, 0);
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(formattedInstant).toEqual("2001-01-01 00:00:00");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can format 2001-01-01 01:02:03 as YYYY-MM-DD HH:mm:ss", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1, 1, 2, 3);
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(formattedInstant).toEqual("2001-01-01 01:02:03");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });


    it("can format 2001-01-01 13:12:14 as YYYY-MM-DD HH:mm:ss", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1, 13, 12, 14);
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(formattedInstant).toEqual("2001-01-01 13:12:14");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can format 2001-01-01 13:12:14 as MM/DD/YYYY h:mm:ss tt", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1, 13, 12, 14);
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "MM/DD/YYYY h:mm:ss tt");
            expect(formattedInstant).toEqual("01/01/2001 1:12:14 PM");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
