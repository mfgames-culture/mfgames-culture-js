import * as Big from "big.js";
import * as data from "mfgames-culture-data";
import { PropertyCultureDataProvider } from "../providers/PropertyCultureDataProvider";

/**
 * Provides both a promise- and sync-based provider for cultural data.
 */
export var provider = new PropertyCultureDataProvider(data.combined);

/**
 * Calculates the Julian Date Number from a given year, month, day.
 */
export function getJulian(
    year: number,
    month: number,
    day: number,
    hour: number = 0,
    minute: number = 0,
    second: number = 0): any
{
    // Figure out the data using the formula from Wikipedia.
    var a = Big(Math.floor((14 - month) / 12));
    var y = Big(year).plus(4800).minus(a);
    var m = Big(month).plus(Big(12).times(a)).minus(3);
    var t = Big(hour).div(24).plus(Big(minute).div(24).div(60)).plus(Big(second).div(24).div(60).div(60));
    var jdn = Big(day).plus(Math.floor(Number(m.times(153).plus(2).div(5)))).plus(y.times(365)).plus(Math.floor(Number(y.div(4)))).minus(Math.floor(Number(y.div(100)))).plus(Math.floor(Number(y.div(400)))).minus(32045).plus(t);
    return jdn.minus(0.5);
}

/**
 * Calculates the Julian Date Number from just hours, minutes, seconds.
 */
export function getJulianTime(
    hour: number = 0,
    minute: number = 0,
    second: number = 0): number
{
    // Figure out the data using the formula from Wikipedia.
    var t = hour / 24 + minute / 24 / 60 + second / 24 / 60 / 60;
    return t - 0.5;
}
