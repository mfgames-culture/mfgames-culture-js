import * as expect from "expect";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    it("instant formats has JD first", function(done)
    {
        try
        {
            var formats = culture.getInstantFormats();
            expect(formats.indexOf("JD")).toEqual(0);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("instant formats has 'YYYY-MM-DD HH:mm:ss'", function(done)
    {
        try
        {
            var formats = culture.getInstantFormats();
            expect(formats.indexOf("YYYY-MM-DD HH:mm:ss")).toBeGreaterThan(0);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("period formats has JD first", function(done)
    {
        try
        {
            var formats = culture.getPeriodFormats();
            expect(formats.indexOf("JD")).toEqual(0);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
