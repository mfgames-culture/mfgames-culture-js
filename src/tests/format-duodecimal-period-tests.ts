import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    it("format 0 JDN as d:hh:mm:ss", function(done)
    {
        try
        {
            var instant = culture.periods.get(0);
            var formattedInstant = culture.format(instant, "d:hh:mm:ss");
            expect(formattedInstant).toEqual("0:00:00:00");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("format 0.5 JDN as d:hh:mm:ss", function(done)
    {
        try
        {
            var instant = culture.periods.get(0.5);
            var formattedInstant = culture.format(instant, "d:hh:mm:ss");
            expect(formattedInstant).toEqual("0:12:00:00");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("format 0 JDN as h:mm:ss", function(done)
    {
        try
        {
            var instant = culture.periods.get(0);
            var formattedInstant = culture.format(instant, "h:mm:ss");
            expect(formattedInstant).toEqual("0:00:00");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("format 0.5 JDN as h:mm:ss", function(done)
    {
        try
        {
            var instant = culture.periods.get(0.5);
            var formattedInstant = culture.format(instant, "h:mm:ss");
            expect(formattedInstant).toEqual("12:00:00");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
