import * as Big from "big.js";
import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { getJulian, provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    it("julian 01/01/2000", function(done)
    {
        try
        {
            var julian = getJulian(2000, 1, 1);
            var instant = culture.parseInstant("" + julian);
            var calendar = culture.instants;
            var expected = calendar.get(julian);
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("big.js 01/01/2000", function(done)
    {
        try
        {
            var julian = Big(getJulian(2000, 1, 1));
            var instant = culture.parseInstant(julian);
            var calendar = culture.instants;
            var expected = calendar.get(julian);
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("instant 01/01/2000", function(done)
    {
        try
        {
            var julian = getJulian(2000, 1, 1);
            var instant1 = culture.parseInstant(julian);
            var instant = culture.parseInstant(instant1);
            var calendar = culture.instants;
            var expected = calendar.get(julian);
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("number 01/01/2000", function(done)
    {
        try
        {
            var instant = culture.parseInstant("01/01/2000", "MM/DD/YYYY");
            var calendar = culture.instants;
            var expected = calendar.get(getJulian(2000, 1, 1));
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("01/01/2000 unspecified", function(done)
    {
        try
        {
            var instant = culture.parseInstant("01/01/2000");
            var calendar = culture.instants;
            var expected = calendar.get(getJulian(2000, 1, 1));
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("Feb 03, 12", function(done)
    {
        try
        {
            var instant = culture.parseInstant("Feb 03, 12", "MMM DD, YY");
            var calendar = culture.instants;
            var expected = calendar.get(getJulian(2012, 2, 3));
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("Feb 03, 12 unspecified", function(done)
    {
        try
        {
            var instant = culture.parseInstant("Feb 03, 12");
            var calendar = culture.instants;
            var expected = calendar.get(getJulian(2012, 2, 3));
            expect(instant).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
