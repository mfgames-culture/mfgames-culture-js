import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    function run(done)
    {
        try
        {
            var input = this.test.title;
            var instant = culture.parsePeriod(input, "d:hh:mm:ss");
            var format = culture.format(instant, "d:hh:mm:ss");
            expect(format).toEqual(input);
            done();
        } catch (exception)
        {
            done(exception);
        }
    }

    it("0:00:00:00", run);
    it("0:01:00:00", run);
    it("0:00:01:00", run);
    it("0:00:00:01", run);
    it("0:01:02:03", run);
});
