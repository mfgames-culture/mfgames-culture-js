import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    function run(done)
    {
        try
        {
            // Pull out the components.
            var test = this.test.title.split(" ");
            var dateFormat = test[0] + " " + test[1];
            var periodFormat = test[3];
            var expected = test[5] + " " + test[6];

            // Convert both elements into types.
            var date = culture.parseInstant(dateFormat);
            var period = culture.parsePeriod(periodFormat);

            // Add the two together.
            var stop = culture.addPeriod(date, period);
            var stopFormat = culture.formatInstant(stop, "YYYY-MM-DD HH:mm:ss");

            // Figure out if we expected the results.
            expect(stopFormat).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    }

    it("2017-02-15 00:00:00 + 1:00:00:00 => 2017-02-16 00:00:00", run);
    it("2017-02-15 00:00:00 + 0:01:00:00 => 2017-02-15 01:00:00", run);
    it("2017-02-15 00:00:00 + 0:00:01:00 => 2017-02-15 00:01:00", run);
    it("2017-02-15 00:00:00 + 0:00:00:01 => 2017-02-15 00:00:01", run);
    it("2017-02-15 00:00:00 + 1:00:00 => 2017-02-15 01:00:00", run);
    it("2017-02-15 00:00:00 + 0:01:00 => 2017-02-15 00:01:00", run);
    it("2017-02-15 00:00:00 + 0:00:01 => 2017-02-15 00:00:01", run);
});
