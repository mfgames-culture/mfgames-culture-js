import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    function run(done: (callback?: any) => void)
    {
        try
        {
            // Pull out the components.
            var test = this.test.title.split(" ");
            var startFormat = test[0].replace("T", " ");
            var stopFormat = test[2].replace("T", " ");
            var expectedFormat = test[4].replace("T", " ");

            // Convert both elements into the appropriate types.
            var start = startFormat.indexOf(" ") >= 0
                ? culture.parseInstant(startFormat)
                : culture.parsePeriod(startFormat);
            var stop = stopFormat.indexOf(" ") >= 0
                ? culture.parseInstant(stopFormat)
                : culture.parsePeriod(stopFormat);

            // Get the first item.
            var first = culture.getLast(start, stop);
            var firstFormat = culture.formatInstant(first, "YYYY-MM-DD HH:mm:ss");

            // Figure out if we expected the results.
            expect(firstFormat).toEqual(expectedFormat);
            done();
        } catch (exception)
        {
            done(exception);
        }
    }

    it("2017-02-15T00:00:00 ? 2017-02-16T00:00:00 => 2017-02-16T00:00:00", run);
    it("2017-02-16T00:00:00 ? 2017-02-15T00:00:00 => 2017-02-16T00:00:00", run);
    it("2017-02-15T00:00:00 ? 1:00:00:00 => 2017-02-16T00:00:00", run);
});
