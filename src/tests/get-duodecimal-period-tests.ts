import * as Big from "big.js";
import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Calendar, Period, TemporalType } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var cal = new Calendar<Period>(
        TemporalType.period,
        provider.getCalendarSync("duodecimal-period"));

    it("can get components for 1", function(done)
    {
        try
        {
            var julian = 1.0;
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.period,
                julian: Big(1),
                totalDays: 1,
                totalHours: 24,
                totalMinutes: 1440,
                totalSeconds: 86400,
                days: 1,
                hours: 0,
                minutes: 0,
                seconds: 0,
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can get components for 1.5", function(done)
    {
        try
        {
            var julian = 1.5;
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.period,
                julian: Big(1.5),
                totalDays: 1.5,
                totalHours: 36,
                totalMinutes: 2160,
                totalSeconds: 129600,
                days: 1,
                hours: 12,
                minutes: 0,
                seconds: 0,
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can get components for -1.5", function(done)
    {
        try
        {
            var julian = -1.5;
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.period,
                julian: Big(-1.5),
                totalDays: -1.5,
                totalHours: -36,
                totalMinutes: -2160,
                totalSeconds: -129600,
                days: -1,
                hours: -12,
                minutes: 0,
                seconds: 0,
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
