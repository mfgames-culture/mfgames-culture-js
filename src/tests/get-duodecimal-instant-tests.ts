import * as Big from "big.js";
import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Calendar, Instant, TemporalType } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var cal = new Calendar<Instant>(
        TemporalType.instant,
        provider.getCalendarSync("duodecimal"));

    it("can get components for 00:00:00", function(done)
    {
        try
        {
            var julian = 0.5 + 0 / 24 + 0 / 1440 + 0 / 86400;
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.instant,
                julian: Big(0.5),
                hour24: 0,
                hour12: 0,
                hourMinute: 0,
                minuteSecond: 0,
                meridiem: 0
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can get components for 06:00:00", function(done)
    {
        try
        {
            var julian = 0.5 + 6 / 24 + 0 / 1440 + 0 / 86400;
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.instant,
                julian: Big(0.75),
                hour24: 6,
                hour12: 6,
                hourMinute: 0,
                minuteSecond: 0,
                meridiem: 0
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can get components for 12:00:00", function(done)
    {
        try
        {
            var julian = 0.5 + 12 / 24 + 0 / 1440 + 0 / 86400;
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.instant,
                julian: Big(1),
                hour24: 12,
                hour12: 0,
                hourMinute: 0,
                minuteSecond: 0,
                meridiem: 1
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can get components for 13:14:12", function(done)
    {
        try
        {
            var julian = Big(0.5)
                .plus(Big(13).div(24))
                .plus(Big(14).div(1440))
                .plus(Big(12).div(86400));
            var point = cal.get(julian);
            expect(point).toEqual({
                type: TemporalType.instant,
                julian,
                hour24: 13,
                hour12: 1,
                hourMinute: 14,
                minuteSecond: 12,
                meridiem: 1
            });
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
