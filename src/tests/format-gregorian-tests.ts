import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { getJulian, provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    it("can't format 2001-01-01 as INVALID", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1);
            var calendar = culture.instants;
            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "INVALID");
            done(new Error("Incorrectly formatted a date as INVALID."));
        } catch (exception)
        {
            done();
        }
    });

    it("can format 2001-01-01 as MM/DD/YYYY", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1);
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "MM/DD/YYYY");
            expect(formattedInstant).toEqual("01/01/2001");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can format 2001-01-01 as MMM DD, YY", function(done)
    {
        try
        {
            var julian = getJulian(2001, 1, 1);
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "MMM DD, YY");
            expect(formattedInstant).toEqual("Jan 01, 01");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("can format 2001-01-01 as JD", function(done)
    {
        try
        {
            var julian = 2451910.5;
            var calendar = culture.instants;
            expect(calendar).toExist();

            var instant = calendar.get(julian);
            var formattedInstant = culture.formatInstant(instant, "JD");
            expect(formattedInstant).toEqual("2451910.5");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
