import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    function run(done)
    {
        try
        {
            // Pull out the components.
            var test = this.test.title.split(" ");
            var startFormat = test[0] + " " + test[1];
            var stopFormat = test[2] + " " + test[3];
            var expected = test[5];

            // Convert both elements into types.
            var start = culture.parseInstant(startFormat);
            var stop = culture.parseInstant(stopFormat);

            // Get the elapsed time.
            var elapsed = culture.getElapsed(start, stop);
            var elapsedFormat = culture.format(elapsed, "d:hh:mm:ss");

            // Figure out if we expected the results.
            expect(elapsedFormat).toEqual(expected);
            done();
        } catch (exception)
        {
            done(exception);
        }
    }

    it("2017-02-15 00:00:00 2017-02-15 00:00:00 => 0:00:00:00", run);
    it("2017-02-15 00:00:00 2017-02-16 00:00:00 => 1:00:00:00", run);
    it("2017-02-15 00:00:00 2017-02-15 01:00:00 => 0:01:00:00", run);
    it("2017-02-15 00:00:00 2017-02-15 00:02:00 => 0:00:02:00", run);
    it("2017-02-15 00:00:00 2017-02-15 00:00:03 => 0:00:00:03", run);
});
