import * as expect from "expect";
import "mocha";
import * as path from "path";
import { Culture } from "../index";
import { provider } from "./helper";

describe(path.basename(__filename), function()
{
    var culture = new Culture(provider.getCultureSync("en-US"));

    it("2015-01-02 13:14:15", function(done)
    {
        try
        {
            var instant = culture.parseInstant("2015-01-02 13:14:15");
            var format = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(format).toEqual("2015-01-02 13:14:15");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("2015-01-02 13:14:14", function(done)
    {
        try
        {
            var instant = culture.parseInstant("2015-01-02 13:14:14");
            var format = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(format).toEqual("2015-01-02 13:14:14");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("2015-01-02 13:14:13", function(done)
    {
        try
        {
            var instant = culture.parseInstant("2015-01-02 13:14:13");
            var format = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(format).toEqual("2015-01-02 13:14:13");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("2015-01-02 13:14:12", function(done)
    {
        try
        {
            var instant = culture.parseInstant("2015-01-02 13:14:12");
            var format = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(format).toEqual("2015-01-02 13:14:12");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("2015-01-02 13:14:11", function(done)
    {
        try
        {
            var instant = culture.parseInstant("2015-01-02 13:14:11");
            var format = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(format).toEqual("2015-01-02 13:14:11");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });

    it("2015-01-02 13:14:11 x2", function(done)
    {
        try
        {
            var instant = culture.parseInstant("2015-01-02 13:14:11");
            var format = culture.formatInstant(instant, "YYYY-MM-DD HH:mm:ss");
            expect(format).toEqual("2015-01-02 13:14:11");
            done();
        } catch (exception)
        {
            done(exception);
        }
    });
});
