import * as Big from "big.js";

import { getPartialDays, toBig } from "./common";
import * as interfaces from "./interfaces";

export type CycleValue = number | string | BigJsLibrary.BigJS
    | interfaces.InstantOrPeriod;

export class Calendar<T extends interfaces.InstantOrPeriod> {
    public data: interfaces.CalendarData;
    public type: interfaces.TemporalType;

    constructor(type: interfaces.TemporalType, data: interfaces.CalendarData)
    {
        this.type = type;
        this.data = data;
    }

    public get(input: CycleValue): T
    {
        // Normalize the Julian date to milliseconds because it is easier to
        // handle base-12 and base-3 elements from there. Since we have
        // high-precision values (seconds in a day don't divide evenly),
        // we centralize the processing.
        let julian = toBig(input);

        // This system can't handle negative values. To get around that, we
        // reverse a negative julian, process it, then then negate the cycles
        // (which we can do since we know there are only two proeprties that
        // aren't cycles in the resulting object).
        const isNegative = julian.lt(0);

        if (isNegative)
        {
            julian = julian.times(-1);
        }

        // Set up the default instant. We use the toJulianDate instead of the
        // input directly to ensure we are converting exactly what we intend.
        const instant = { julian, type: this.type };

        // Go through each of the cycles and calculate each one. We will reset
        // the julian date for each one since each of these cycles is calculated
        // independently.
        if (this.data.cycles)
        {
            for (const cycle of this.data.cycles)
            {
                this.calculateCycle(cycle, julian, instant);
            }
        }

        // If it was negative, then reverse all the values.
        if (isNegative)
        {
            instant.julian = instant.julian.times(-1);

            for (const key in instant)
            {
                if (key === "type" || key === "julian")
                {
                    continue;
                }

                if (!instant.hasOwnProperty(key))
                {
                    continue;
                }
                if (instant[key] === 0)
                {
                    continue;
                }

                instant[key] *= -1;
            }
        }

        // Return the resulting calendar instant.
        return instant as T;
    }

    public getJulian(instant: any): number
    {
        const bigJulian = this.getBigJulian(instant);
        return Number(bigJulian);
    }

    public getBigJulian(instant: any): BigJsLibrary.BigJS
    {
        // Loop through the top-level cycles and see if any of these will work.
        // If they do, calculate the Julian Date. At the moment, we include all
        // of the root elements which will handle composite calendars.
        let julian = Big(0);
        const working = {};

        if (this.data.cycles)
        {
            for (const cycle of this.data.cycles)
            {
                const cycleJulian = this.getCycleJulian(instant, cycle, working);
                julian = julian.plus(cycleJulian);
            }
        }

        // Return the resulting julian.
        return julian;
    }

    private getCycleJulian(
        instant: any,
        cycle: interfaces.CalendarCycleData,
        working: any): BigJsLibrary.BigJS
    {
        // If we don't have the index for the cycle, then skip it.
        let julian = Big(0);

        if (!(cycle.id in instant))
        {
            return julian;
        }

        // Figure out the type of cycle, since that will determine how we
        // calcualte it.
        const index = instant[cycle.id];

        switch (cycle.type)
        {
            case interfaces.CycleType.repeat:
                julian = this.getRepeatCycleJulian(instant, cycle, working);
                break;
            case interfaces.CycleType.sequence:
                julian = this.getSequenceCycleJulian(instant, cycle, working);
                break;
            case interfaces.CycleType.fraction:
                julian = this.getFractionCycleJulian(instant, cycle, working);
                break;
            default:
                throw new Error(
                    `Cannot process unknown cycle type: ${cycle.type}.`);
        }

        // Once we go through this, then go through the child cycles.
        if (cycle.cycles)
        {
            for (const child of cycle.cycles)
            {
                const childJulian =
                    this.getCycleJulian(instant, child, working);
                julian = julian.plus(childJulian);
            }
        }

        // If we have an offset, modify it.
        if (cycle.offset)
        {
            julian = julian.minus(toBig(cycle.offset));
        }

        // Pull out the index and calculate the values.
        return julian;
    }

    private getFractionCycleJulian(
        instant: any,
        cycle: interfaces.CalendarCycleData,
        working: any): BigJsLibrary.BigJS
    {
        // Fractions are pretty easy to calculate since we multiply the index
        // times the fractional amount.
        const index = instant[cycle.id];
        const value = toBig(cycle.value);
        const julian = toBig(index).div(value);

        // Return the resulting Julian date.
        return julian;
    }

    private getRepeatCycleJulian(
        instant: any,
        cycle: interfaces.CalendarCycleData,
        working: any): BigJsLibrary.BigJS
    {
        // Start with the base cycle and zero out the index of the working set.
        const index = instant[cycle.id];
        let julian = Big(0);
        working[cycle.id] = 0;

        // Loop through the types, processing each one until we run out.
        while (working[cycle.id] <= index)
        {
            // Calculate the length of the next cycle by finding the next one.
            let found = false;
            const lengths = cycle.lengths;

            if (!lengths)
            {
                throw new Error("There were no lengths in cycle.");
            }

            for (const lengthIndex in lengths)
            {
                // Ignore non-indexes.
                if (!lengths.hasOwnProperty(lengthIndex))
                {
                    continue;
                }

                // If this cycle would have put us over, skip it.
                const length = lengths[lengthIndex];

                if (length.count + working[cycle.id] > index) { continue; }

                // Calculate the length of this length. If this is less than or
                // equal to the Julian Date, we need to keep it.
                const next = this.calculateLength(length, working);

                if (next.lte(0)) { continue; }

                working[cycle.id] += length.count;
                julian = julian.plus(next);
                found = true;
                break;
            }

            // If we fall through, then there is something wrong so break out.
            if (!found) { break; }
        }

        // Return the resulting Julian date.
        return julian;
    }

    private getSequenceCycleJulian(
        instant: any,
        cycle: interfaces.CalendarCycleData,
        working: any): BigJsLibrary.BigJS
    {
        // Start with the base cycle and zero out the index of the working set.
        const index = instant[cycle.id];
        let julian = Big(0);
        working[cycle.id] = 0;

        // Loop through the sequence lengths until we exceed our limit.
        for (let cycleIndex = 0; cycleIndex < index; cycleIndex++)
        {
            // Update the working with the current cycle.
            working[cycle.id] = cycleIndex;

            // If we don't have a length, blow up.
            if (!cycle.lengths)
            {
                throw new Error("Cannot find lengths in cycle: " + JSON.stringify(cycle));
            }

            // Calculate the length of this sequence.
            const length = cycle.lengths[cycleIndex];
            const next = this.calculateLength(length, instant);

            julian = julian.plus(next);
        }

        // Return the resulting Julian date.
        return julian;
    }

    private calculateCycle(
        cycle: interfaces.CalendarCycleData,
        julian: BigJsLibrary.BigJS,
        instant: any): void
    {
        // If we have an offset, modify the date by it.
        if (cycle.offset)
        {
            julian = julian.plus(toBig(cycle.offset));
        }

        // Check to see if we are only dealing with days.
        if (cycle.partialDaysOnly)
        {
            julian = getPartialDays(julian);
        }

        // Figure out what to do based on the type of the cycle.
        switch (cycle.type)
        {
            case interfaces.CycleType.repeat:
                this.calculateRepeatCycle(cycle, julian, instant);
                break;

            case interfaces.CycleType.calculate:
                this.calculateCalculateCycle(cycle, julian, instant);
                break;

            case interfaces.CycleType.sequence:
                this.calculateSequenceCycle(cycle, julian, instant);
                break;

            case interfaces.CycleType.fraction:
                this.calculateFractionCycle(cycle, julian, instant);
                break;

            default:
                throw new Error(
                    "Cannot handle cycle type of " + cycle.type + ".");
        }
    }

    private calculateCalculateCycle(
        cycle: interfaces.CalendarCycleData,
        julian: BigJsLibrary.BigJS,
        instant: any): void
    {
        const ref = cycle.ref;

        if (!ref)
        {
            throw new Error("cannot find ref from cycle: " + JSON.stringify(cycle));
        }

        const index = instant[ref];
        const value = cycle.value;

        if (!value)
        {
            throw new Error("Cannot find value from instant[" + ref + "].");
        }

        switch (cycle.operation)
        {
            case interfaces.CycleOperationType.mod:
                instant[cycle.id] = index % value;
                break;
            case interfaces.CycleOperationType.div:
                instant[cycle.id] = Math.floor(index / value);
                break;
            case interfaces.CycleOperationType.mult:
                instant[cycle.id] = index * value;
                break;
        }

        // If we have additional cycles, we want to calculate them recursively.
        if (cycle.cycles)
        {
            for (const child of cycle.cycles)
            {
                this.calculateCycle(child, julian, instant);
            }
        }
    }

    private calculateFractionCycle(
        cycle: interfaces.CalendarCycleData,
        julian: BigJsLibrary.BigJS,
        instant: any): void
    {
        // Figure out what the individual units are based on. We use this times
        // the various values to figure out which index to use. We do this
        // because even BigJS can't handle repeated `x -= * 1 / 86400` where it
        // can handle multiplying values.
        const value = toBig(cycle.value);
        let index = toBig(0);

        while (julian.minus(index.plus(1).div(value)).gte(0))
        {
            index = index.plus(1);
        }

        // We found the index, so use that.
        const length = index.div(value);
        instant[cycle.id] = parseInt(index.toFixed(0), 10);
        julian = julian.minus(length);

        // If we have additional cycles, we want to calculate them recursively.
        if (cycle.cycles)
        {
            for (const child of cycle.cycles)
            {
                this.calculateCycle(child, julian, instant);
            }
        }
    }

    private calculateRepeatCycle(
        cycle: interfaces.CalendarCycleData,
        julian: BigJsLibrary.BigJS,
        instant: any): void
    {
        // Start with the zero index.
        instant[cycle.id] = 0;

        // Loop through the various lengths until we encounter a length that
        // exceeds the remaining Julian Date.
        while (julian.gt(0))
        {
            // Calculate the length of the next cycle by finding the next one.
            let found = false;
            const lengths = cycle.lengths;

            if (!lengths)
            {
                throw new Error("There were no lengths in cycle.");
            }

            for (const lengthIndex in lengths)
            {
                // Ignore non-indexes.
                if (!lengths.hasOwnProperty(lengthIndex))
                {
                    continue;
                }

                // Calculate the length of this length. If this is less than or
                // equal to the Julian Date, we need to keep it.
                const length = lengths[lengthIndex];
                const next = this.calculateLength(length, instant);

                // If we got a zero, that means this one didn't apply at all.
                if (next.lte(0)) { continue; }

                // If the Julian days can fit within the amount, then we
                // use this one and then stop processing.
                if (next.lte(julian))
                {
                    instant[cycle.id] += length.count;
                    julian = julian.minus(next);
                    found = true;
                    break;
                }

                // If we are not a fast-forward block, we need to stop
                // processing since the next element may be less than the
                // current one (if we have a calendar with fall  days instead
                // of leap days) and it would be caught by that.
                if (length.stopIfValid)
                {
                    break;
                }
            }

            // If we fall through, then there is something wrong so break out.
            if (!found) { break; }
        }

        // If we have additional cycles, we want to calculate them recursively.
        if (cycle.cycles)
        {
            for (const child of cycle.cycles)
            {
                this.calculateCycle(child, julian, instant);
            }
        }
    }

    private calculateSequenceCycle(
        cycle: interfaces.CalendarCycleData,
        julian: BigJsLibrary.BigJS,
        instant: any): void
    {
        // Start with the zero index.
        instant[cycle.id] = 0;

        // Loop through the sequences until we exceed our limit.
        const lengths = cycle.lengths;

        if (!lengths)
        {
            throw new Error("There were no lengths in cycle.");
        }

        for (const lengthIndex in lengths)
        {
            if (!lengths.hasOwnProperty(lengthIndex))
            {
                continue;
            }

            // Calculate the length of this length. If this is less than or
            // equal to the Julian Date, we need to keep it and move to the
            // next.
            const length = lengths[lengthIndex];
            const next = this.calculateLength(length, instant);

            if (next.lte(0) || next.gt(julian)) { break; }

            // Adjust the instant cycle index and move to the next.
            instant[cycle.id]++;
            julian = julian.minus(next);

            // If we hit zero, we're done.
            if (julian.lte(0)) { break; }
        }

        // If we have additional cycles, we want to calculate them recursively.
        if (cycle.cycles)
        {
            for (const child of cycle.cycles)
            {
                this.calculateCycle(child, julian, instant);
            }
        }
    }

    private calculateLength(
        length: interfaces.CalendarLengthData,
        instant: any): BigJsLibrary.BigJS
    {
        // See if we have "single", which means a choice between multiple
        // lengths.
        if (length.single)
        {
            // Loop through the single lengths until we find one that is
            // applicable.
            for (const single of length.single)
            {
                // Get the ref, if we have one.
                let singleValue = 1;
                let singleIndex = 0;

                if (single.ref)
                {
                    singleIndex = instant[single.ref];
                }

                if (single.value)
                {
                    singleValue = single.value;
                }

                switch (single.operation)
                {
                    case interfaces.CycleOperationType.mod:
                        if (singleIndex % singleValue !== 0) { continue; }
                        break;

                    case interfaces.CycleOperationType.div:
                        if (Math.floor(singleIndex / singleValue) !== 0)
                        {
                            continue;
                        }

                        break;

                    case interfaces.CycleOperationType.mult:
                        if (singleIndex * singleValue !== 0) { continue; }
                        break;
                }

                return toBig(single.julian);
            }
        }

        // If we have an operation, then we need to calculate this. If the
        // operation doesn't match, then return 0 to skip the cycle.
        if (length.operation)
        {
            const ref = length.ref;

            if (!ref)
            {
                throw new Error("calculateLength: cannot find ref from cycle: " + JSON.stringify(length));
            }

            const index = instant[ref];
            const value = length.value;

            if (!value)
            {
                throw new Error("calculateLength: cannot find value from instant[" + ref + "].");
            }

            switch (length.operation)
            {
                case interfaces.CycleOperationType.mod:
                    if (index % value !== 0) { return Big(0); }
                    break;

                case interfaces.CycleOperationType.div:
                    if (Math.floor(index / value) !== 0) { return Big(0); }
                    break;

                case interfaces.CycleOperationType.mult:
                    if (index * value !== 0) { return Big(0); }
                    break;
            }
        }

        // We have a valid value, so return the results.
        return toBig(length.julian);
    }
}
