import { MemoryCultureProviderBase } from "./MemoryCultureProviderBase";

/**
 * Implements a culture data provider from an object where the keys in the
 * hash are the culture identifiers.
 */
export class PropertyCultureDataProvider extends MemoryCultureProviderBase
{
    private components: any;

    constructor(components: any)
    {
        super();
        this.components = components;
    }

    protected getComponent(id: string): any
    {
        if (id in this.components)
        {
            return this.components[id];
        }

        throw new Error("Cannot find component ID: '" + id + "'.");
    }
}
