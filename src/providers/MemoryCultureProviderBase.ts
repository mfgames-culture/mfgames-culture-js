import { mergeCalendars } from "../common";
import * as interfaces from "../interfaces";

/**
 * Implements the common functional for memory-based providers.
 */
export abstract class MemoryCultureProviderBase implements
    interfaces.PromiseCultureDataProvider,
    interfaces.SyncCultureDataProvider
{
    public getCulturePromise(id: string): Promise<interfaces.CultureData>
    {
        return new Promise<interfaces.CultureData>((resolve, _error) =>
        {
            resolve(this.getCultureSync(id));
        });
    }

    public getCultureSync(id: string): interfaces.CultureData
    {
        // Load the basic cultural data into memory.
        const culture = this.getComponent(id) as interfaces.CultureData;

        // If we don't have a instants calendar, we need to load it.
        if (culture.instants && !culture.instants.calendarData)
        {
            // Get the instant calendars and merge them together.
            const calendars = culture.instants.refs
                .map((c) => this.getCalendarSync(c));

            // Set the internal data for the temporal.
            culture.instants.calendarData = mergeCalendars(calendars);
        }

        // If we don't have a periods calendar, we need to load it.
        if (culture.periods && !culture.periods.calendarData)
        {
            // Get the instant calendars and merge them together.
            const calendars = culture.periods.refs
                .map((c) => this.getCalendarSync(c));

            // Set the internal data for the temporal.
            culture.periods.calendarData = mergeCalendars(calendars);
        }

        // Return the resulting culture.
        return culture;
    }

    public getCalendarPromise(id: string): Promise<interfaces.CalendarData>
    {
        return new Promise<interfaces.CalendarData>((resolve, _error) =>
        {
            resolve(this.getCalendarSync(id));
        });
    }

    public getCalendarSync(id: string): interfaces.CalendarData
    {
        return this.getComponent(id);
    }

    protected abstract getComponent(id: string): any;
}
