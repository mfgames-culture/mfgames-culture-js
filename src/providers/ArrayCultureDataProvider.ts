import * as interfaces from "../interfaces";
import { MemoryCultureProviderBase } from "./MemoryCultureProviderBase";

/**
 * Implements a provider where the culture and temporal components are in an
 * array.
 */
export class ArrayCultureDataProvider extends MemoryCultureProviderBase
{
    private components: interfaces.ComponentData[];

    constructor(components: interfaces.ComponentData[])
    {
        super();
        this.components = components;
    }

    protected getComponent(id: string): any
    {
        for (const component of this.components)
        {
            if (component.id === id)
            {
                return component;
            }
        }

        throw new Error("Cannot find component ID: '" + id + "'.");
    }
}
